(defun area (a b h)
 (* h (/ (+ a b) 2.0)))

(defun integral (f a b s)
 (cond
 	((> a b) 0)
  (t (+ (area (f a) (f (+ a s)) s) (integral f (+ a s) b s)))))

(defun f (x)
 (* x x))

(print (integral #'f 0 1 0.0001))
(print (integral #'f 0 2 0.0001))
(print (integral #'f 0 3 0.0001))
