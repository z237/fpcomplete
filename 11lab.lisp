(defun mass-beheading (&rest lists)
 	(if lists
 	 (cons (caar lists) (apply #'mass-beheading (cdr lists)))))
; cons - соединяет
; caar - берет первый элемент первого списка
; cdr - весь список кроме первого элемента

(print(mass-beheading '(1 2 3) '(4 5 6) '(7 8 9)))
; поменять(print(mass-beheading '(1 2) '(4 5 6) '(7 8 9)))
; (print(mass-beheading '(1) '(4) '(9)))
