live(muha, goruha).
live(komar, piskun).
live(mishka, pogrizuha).
live(lyagushka, kvakushka).
live(zaunok, krivonog).
live(lisa, krasa).
live(volk, hvatish).
notLive(medved, prignetish).

%	a) live(mishka, pogrizuha).
%	b) live(volk, X).
%	c) live(X, krivonog).
% d) notLive(M, P).

%	Вопросы:
%	live(lyagushka, _).
%	live(lisa, X).
%	live(X, goruha).

print :- 
	notLive(Y, _),
	writeln(Y),
	live(X, _), 
	writeln(X), 
	fail.