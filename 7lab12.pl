gen(N, FOut, GOut, Prev) :-
	N is 0 -> !
;	R is random(10),
	write(FOut, R), write(FOut, ' '),
	NewN is N - 1,
	printF(R, Prev, GOut),
	gen(NewN, FOut, GOut, R).

printF(N1, N2, GOut) :-
	Mult is N1 * N2,
	Mult < 0 -> true
; Mult is N1 * N2, 
  write(GOut, Mult), write(GOut, ' ').

print(N) :-
	open('f.txt', write, FOut),
	open('g.txt', write, GOut),
	gen(N, FOut, GOut, -1),
	close(FOut), close(GOut).