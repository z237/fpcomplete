husband(1, boris, gennadievich, ivanov, 1945, 10000).
husband(2, ararat, faserovich, bulkin, 1987, 15000).
husband(3, kurgan, albertovic, promenade, 1969, 20000).
husband(4, vasiliy, kukich, julkin, 1971, 30000).
husband(5, dmitriy, glebovich, kuritov, 1983, 7000).
husband(6, andrey, dmitrievich, pelmenev, 1964, 13000).

wife(1, alisa, olegovna, ivanova, 1945, 10000).
wife(2, malina, glebovna, bulkina, 1984, 7000).
wife(3, marina, romanovna, promenade, 1971, 12000).
wife(4, matilda, varisovna, julkina, 1973, 13333).
wife(5, olga, larisovna, kuritova, 1981, 14000).
wife(6, alena, borisovna, pelmeneva, 1964, 25000).

child(1, 1, gena, gennadievich, ivanov, 1963, 2).
child(2, 1, lena, gennadievna, ivanova, 1963, 1).
child(3, 2, jenya, gennadievich, bulkin, 2005, 0).
child(4, 3, vasgen, kurganovich, promenade, 2004, 0).
child(5, 4, brat, vasilevich, julkin, 1992, 6).
child(6, 4, sestra, vasilevna, julkina, 1992, 5).

findTwins :- 
	child(Id, _, _, _, _, _, X), 
	X \= 0, 
	write(Id),
	write('<=>'),
	writeln(X), 
	fail.

findBorn(Year) :- 
	child(Id, _, _, _, _, Year, _),
	writeln(Id),
	fail.

findWifes(Income) :- 
	wife(_, I, O, F, _, Inc), 
	Income < Inc,
	write(I), write(' '),
	write(O), write(' '),
	writeln(F),
	fail. 

incr(X, X1) :-
	X1 is X + 1.

countChildren(Id, Count) :- 
	child(_, Id, _, _, _, _, _),
	incr(Count, Count1),
	writeln(Count1),
	fail.

findChildren(Count) :-
	husband(Id, _, _, F, _, _),
	aggregate_all(count, child(_, Id, _, _, _, _, _), Cnt),
	Count is Cnt,
	writeln(F),
	fail.
