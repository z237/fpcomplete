make(N, List) :-
	N < 1 ->
	printList(List)
;	nth1(1, List1, N, List),
	NewN is N - 1,
	make(NewN, List1).

makeList(N) :-
	L = [],
	make(N, L).

printList([X|List]) :-
	printList(List),
	write(X), write(' ').

printList([]).
