(defun print-horizontal (n)
 	(loop for i from 1 to n
 	 	do (format t "*"))
 	(format t "~%"))

(defun print-vertical (n)
 (format t "*")
 (loop for i from 1 to (- n 2)
  do (format t " "))
 (format t "*")
 (format t "~%"))

(defun print-square (n)
 (print-horizontal n)
 (loop for i from 1 to (- n 2)
  do (print-vertical n))
 (print-horizontal n))

	(setq n (read))
	(cond 
	 ((eq n 1) (format t "*"))
	 (t (print-square n)))
