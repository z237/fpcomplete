%	summ(2, 0, 2) = 3669866
summ(X, Sum, Fact) :-
	X > 10 -> writeln(Sum)
;	NewX is X + 2,
	NewFact is Fact * (X + 1) * (X + 2),
	NewSum is Sum + Fact,
%	writeln(NewSum),
	summ(NewX, NewSum, NewFact).