% database kolobok
left(kolobok, dedushka).
left(kolobok, babushka).
left(kolobok, zayac).
left(kolobok, volk).
left(kolobok, medved).
notLeft(kolobok, lisa).

print :- 
	notLeft(X, Z), 
	writeln(X), 
	writeln(Z), 
	left(_, Y), 
	writeln(Y), 
	fail.