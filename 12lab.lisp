(defun nested-sum (lists)
 (cond
  ((null lists) 0)
	((atom lists) lists)
  (t (+ (nested-sum (car lists)) (nested-sum (cdr lists))))))

(print (nested-sum '(1 (2 ((3))) (4 5))))
(print (nested-sum '(1 (2 (())) (4))))
(print (nested-sum '(1 (((3))) (4 -4))))