odd(X) :-
	Mod is X mod 2,
	Mod is 1.

oddRand() :-
	X is random(10), 
	write(X),
	odd(X).

gift(nick, tom, book).
gift(mary, tom, pen).
gift(rick, mary, toy).
gift(bob, pat, toy).

%	Заполнить своими данными (произвольными)
toy(soldatik, 20, 100).
toy(baranka, 10, 1000).
toy(yula, 100, 5).
toy(kukla, 33, 7).

print :-
	toy(Name, Count, Cost),
	write(Name), write(' '),
	write(Count), write(' '),
	writeln(Cost), fail.

printCost :-
	toy(Name, Count, Cost),
	write(Name), write(' - '),
	Sum is Count * Cost,
	writeln(Sum), fail.
