:- encoding(utf8).

add(Country, Name, Height, Y, Conqueror) :-
	assertz(mountains(Country, Name, Height, Y, Conqueror)).

fillDB :-
	add('Эквадор', 'Чимбарасо', 6267, 1880, 'Уимпер'),
	add('Аргентина', 'Аконкагуа', 6960, 1897, 'Цубригген'),
	add('Перу', 'Хуаскаран', 6768, 1908, 'Анни Пекк'),
	add('СССР', 'Пик Ленина', 7134, 1928, 'Рикмерс'),
	add('СССР', 'Пик Коммунизма', 7495, 1933, 'Абалаков'),
	add('Непал', 'Аннапурна', 8078, 1950, 'Герцог'),
	add('Непал/Тибет', 'Эверест', 8848, 1953, 'Хант'),
	add('Пакистан', 'Нанга Парбат', 8126, 1953, 'Херрлингоффер'),
	add('Индия', 'К2', 8611, 1954, 'Дезино'),
	add('Непал/Сикким', 'Кангченджунга', 8598, 1955, 'Браун').

writeFact(Hrebet, Name, Height, Y, Conqueror) :-
	write(Hrebet), write(', '),
	write(Name), write(', '),
	write(Height), write(', '),
	write(Y), write(', '),
	writeln(Conqueror).

printDB :-
	forall(mountains(Hrebet, Name, Height, Y, Conqueror),
	writeFact(Hrebet, Name, Height, Y, Conqueror)).

fromCounty(Country) :-
	mountains(Country, N, H, Y, Cq),
	writeFact(Country, N, H, Y, Cq).

laterThan(Year) :-
	mountains(C, N, H, Y, Cq),
	Y > Year,
	writeFact(C, N, H, Y, Cq).

heightBetween(H1, H2) :-
	mountains(C, N, H, Y, Cq),
	H > H1,
	H < H2,
	writeFact(C, N, H, Y, Cq).

goal :-
	fillDB,
	fromCounty('Пакистан'),
	laterThan(1950),
	heightBetween(8000, 8500).