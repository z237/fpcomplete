% 2 * 4 * .. * 26 = 13! * 2^13 = 51011754393600
% mult(2, 1) - calling recursion
mult(X, Ans) :-
	X > 26 -> writeln(Ans), 
	fail
;	NewX is X + 2,
	Ans1 is Ans * X,
	mult(NewX, Ans1).