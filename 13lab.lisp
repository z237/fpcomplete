(defun flatten(lists)
 (cond
  ((null lists) ())
  ((atom lists) (cons lists ()))
  (t (append (flatten (car lists)) (flatten (cdr lists)))))) 


(print (flatten '(1 (2 ((3))) (4 5))))