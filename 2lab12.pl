sample1(X, Y) :-
	Result is 4 * X + 6 ** Y - 9 * X ** 3,
	writeln(Result).
% sample1(1, 1) = 1
% sample1(2, 2) = -28

sample2(X, Y) :-
	Result is cos(12 * Y) + Y ** X + log(125) / (3 + X),
	writeln(Result). 
% sample2(1, 1) = 3.051
% sample2(2, 2) = 5.39